use this image to host your java servlets using tomcat

start this container by command:
docker run -d -p <host port to bind>:80 virtualapps/opensuse-tomcat

example:
docker run -d -p 80:8080 -p 443:8443 virtualapps/opensuse-tomcat

you can access your web service by pointing your browser to url: http://<hostname>:<host port to bind>

In the above example access url is http://hostname:80 or https://hostname:443

copy your war files to /usr/share/tomcat/webapps/ directory